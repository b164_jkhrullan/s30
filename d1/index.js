const express = require("express");

const mongoose = require("mongoose");

const app = express();

const port = 3001;

mongoose.connect('mongodb+srv://dbUser:dbUser@zuitt.ri5rh.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
)

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


app.use(express.json());

app.use(express.urlencoded({ extended:true }));




const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		
		default: "pending"
	}
})


const Task = mongoose.model("Task", taskSchema);


app.post("/tasks", (req, res) => {
	Task.findOne({ name: req.body.name }, (err, result) => {

		
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");
		} else{
			
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr)
				} else {
					
					return res.status(201).send("New task created")
				}
			})


		}
	})
})

app.get("/tasks", (req, res) => {

	
	Task.find({}, (err, result) => {

		if(err) {
			return console.log(err);
		} else {
			
			return res.status(200).json({
				Message: "from the database",
				dataFromMDB: result
			})
		}
	})
})


//Activity
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res)=> {

	User.findOne({ username : req.body.username }, (err, result) => {

		if(result != null && result.username == req.body.username){

			return res.send("Duplicate username found");

		} else {
			if(req.body.username !== '' && req.body.password !== ''){
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });

                newUser.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr);
                    } else {
                        return res.status(201).send("New user registered");

                    }

                })
            } else {
                return res.send("BOTH username and password must be provided.");
            }			
		}
	})
})






app.listen(port, ()=> console.log(`Server running at port ${port}`));
